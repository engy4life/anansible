#!/bin/bash  

Developing(){
# 
# Create required folders and files.
# Create a playbook named as the project.
	touch $JobID.yml
# Copies the hosts and ansible.cfg files to the project folde
	touch $HOME/anansible/MyAnsibleWork/$JobID/hosts
	touch $HOME/anansible/MyAnsibleWork/$JobID/ansible.cfg
}


# ============ Environment Variables ============= 

SAI="sudo apt install -y "
STD='\033[0;0;39m'
RED='\033[0;41;30m'
# set $D to display date and time 
	D=$(date +"%T")
# Activate created venv
	source ~/anansible/Pythansible/bin/activate
# Start in users home directory
	cd $HOME
	WorkDir=($HOME/anansible/MyAnsibleWork/$JobID/)

# =================== Setup Ansible =====================

InstallAnsible(){
# Setup Ansible
# Install Ansible within a Python Virtual Environment named Pythansible within the anansible folder.
# Update the host OS
	sudo apt update
# Instal venv, allow ansible to use manual password entry with ssh
	$SAI python3.10-venv sshpass software-properties-common openssh-server nano gedit
# create venv in existing ananisble folder	
	python3 -m venv ~/anansible/Pythansible
# Activate created venv
	source ~/anansible/Pythansible/bin/activate
# install ansible via PIP
	pip install ansible
# create a working folder for ansible projects
	mkdir $HOME/anansible/MyAnsibleWork	
# Validate ansible is available
	which ansible
# Validate install by printing version.
    ValidateInstall
# deactivate = deactivate virtual environment
}

ValidateInstall(){
# Validate install by printing version.
    AnsibleVersion=$(ansible --version)
	lastmessage=""$AnsibleVersion
}

# =================== Projects =====================

NewProject(){
# Capture the new request ID data
    read -rp "Enter the given Project Name (leave blank to quit): " JobID 
# validate data was entered.  
    if [ "$JobID" = "" ]; 
    then 
    lastmessage="Please enter a Project Name"
	else 
# Prepare test folder.
    echo "$JobID"
	fi
# Validate folder does not already exist and inform user if so.
if [ ! -f "$WorkDir" ]; 
	then 
# create and enter the new test folder.     
    mkdir -pv $HOME/anansible/MyAnsibleWork/$JobID/
	cd $HOME/anansible/MyAnsibleWork/$JobID
# Create required folders & files.    
#	touch logs.txt
	touch $JobID.yml
	echo -e "[Physical]" >> $HOME/anansible/MyAnsibleWork/$JobID/hosts
# Setup ansible config to use created project folder
	echo "[defaults]
inventory = $HOME/anansible/MyAnsibleWork/$JobID/hosts"  >> $HOME/anansible/MyAnsibleWork/$JobID/ansible.cfg
	lastmessage="Created folder in: $WorkDir" 
#    echo "A1- New Project $WorkDir created - $D $lastmessage" >> $HOME/anansible/MyAnsibleWork/$JobID/logs.txt
	else 
	lastmessage="Sorry $USER, That name already exists, try again."
	NewProject
	fi
	EditFiles
}

LoadProject(){
# require selector menu for displayed folders.
# Display existing projects (list folders). 
    ls $HOME/anansible/MyAnsibleWork

# Capture the request ID data
    read -rp "Enter the Project Name (leave blank to quit): " JobID 
# Create folder based on Request ID.  
    if [ "$JobID" = "" ]; 
    then 
    lastmessage="Please enter a Project Name"
# Prepare test folder.
	else 
# Add JobID to Username to Prepare project name.
    
    echo "Existing files within the $JobID project" 
    cd $HOME/anansible/MyAnsibleWork/$JobID/
	ls
    lastmessage="- Loaded project: $JobID -"
#   echo "B1- Load Project- $D $lastmessage" >> $WorkDir/logs.txt
#	cd $HOME/anansible/MyAnsibleWork/$JobID
	fi
	EditFiles
}

EditFiles(){
# Edit files with Gedit in the background
	gedit $HOME/anansible/MyAnsibleWork/$JobID/ansible.cfg $HOME/anansible/MyAnsibleWork/$JobID/hosts $HOME/anansible/MyAnsibleWork/$JobID/$JobID.yml & disown
}

AddRemoteUser(){	
# Capture the remote user name
    read -rp "Enter the remote_user Name (leave blank to quit): " RemoteUser 
# Create folder based on Request ID.  
    if [ "$RemoteUser" = "" ]; 
    then 
    lastmessage="Please enter a user name"
# Prepare test folder.
	else 
# Add Username to ansible.cfg.
    echo "remote_user: "$RemoteUser >>  $HOME/anansible/MyAnsibleWork/$JobID/ansible.cfg
	fi
}


AddPhysicalServer(){
# Capture the remote user name
    read -rp "Enter the Physical servers name or IP (leave blank to quit): " ServerName 
# Create folder based on Request ID.  
    if [ "$ServerName" = "" ]; 
    then 
    lastmessage="Please enter some data."
# Prepare test folder.
	else 
# Add servers to hosts files.	
	echo $ServerName >> $HOME/anansible/MyAnsibleWork/$JobID/hosts
	fi
}	

ConfigureAnsible(){
echo "some code should be here..."
}

ListHosts(){
# list a specific file in a specific directory
#	cd $WorkDir	
	ansible -i $WorkDir/$JobID/hosts --list-hosts all
# List all hosts	
#	ansible --list-hosts all
# /etc/ansible/hosts	
}

PingAll(){
	cd $HOME/anansible/MyAnsibleWork/$JobID
	ansible -m ping all -k 
}

ValidateYML(){
# Validate your playbooks YML
	read -rp "Enter playbook to validate or blank for current project: " check_playbook
# Validate input	
	if [[ "$check_playbook" == "" ]];
	then 
	check_playbook="~/$JobID.yml"
	lastmessage=" Results: " $ansyml_check
	else
# check yml with yamllint not using due to conflicting output.
#	yml_check=$(yamllint $WorkDir/$check_playbook)
#  check yml with  ansible-lint.
	ansyml_check=$(ansible-lint $WorkDir/$check_playbook)
# notification message to display on the menu screen
	lastmessage=" Results: " $ansyml_check
	fi
}

RunPlaybook(){
# enter playbook name to execute
	read -rp "Enter the name of the Playbook to run or blank for current project : " run_play
# Assign project folder as playbook name if blank
	if [ "$run_play" == ""]
	then 
	ansible-playbook $JobID.yml -vvv
	else
# execute playbook with very, very verbose output
	ansible-playbook -k $run_play.yml -vvv
#	ansible-playbook $WorkDir/$run_play.yml -vvv
	fi
}

CreatePlaybook(){
# enter playbook name to create and execute
	read -rp "Enter your desired Playbook name: " run_play
	new_play=($run_play.yml)
# Open nano to edit new playbook.
	nano $WorkDir$run_play.yml
# validate yml and display errors in new playbook.
	ansyml_check=$(ansible-lint $WorkDir$new_play)
	echo "Ansible YML validation results: " $ansyml_check
# execute playbook
	ansible-playbook --ask-become-pass $WorkDir$new_play -vvv
# notification message to display on the menu screen.	
	lastmessage="Ansible playbook created, validated and executed. " 
}


AutoSSHKey(){
# setup SSH
	printf 'y\n' | ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa
}

AddUser2Group(){
# Add a user to a Group:

	read -rp "Enter the name of the target User: " add_user
	read -rp "Enter the name of the target Group: " add_group
	if [[ "$add_user" == "" ]]
	then 
	lastmessage="Enter a name please....."
	else
	sudo usermod --append --groups $add_group $add_user	
# notification message to display on the menu screen
	lastmessage="added $add_user to $add_group "
	fi
}


pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}


InsBuiEss(){
# Install build essentila for development:	
	lxc exec $newcon -- sudo --login --user ubuntu sh -c "sudo apt install build-essential -y "
}

SetupSSHKeys(){
# generate ssh keys on the control machine. and copy it to other machines.
	printf 'y\n' | ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa
	SSHLogin
}

SSHLogin(){
# Request username and server name to connect to
	read -rp "Enter the name of the target User: " add_user
	read -rp "Enter the target name (leave blank to quit): " SSH_Target
# validate data was entered.  
    if [ "$SSH_Target" = "" ]; 
    then 
    lastmessage="Please enter an SSH target"
	else 
# Login with generated key
#	ansible --become-user BECOME_ubuntu
    ssh $add_user@$SSH_Target mkdir -p .ssh
    cat .ssh/id_rsa.pub | ssh $add_user@$SSH_Target 'cat >> .ssh/authorized_keys'
    ssh $add_user@$SSH_Target
	fi
}

UpdateMenu(){ 
    cd $HOME/anansible
    git add .
    git commit -m "Auto Updates"
    git stash
	git push
    git pull 
}


# ================== Main Menu ====================

MainMenu(){
# Clears the screen
	clear
# Define this menus title.	
	MenuTitle=" - Anansible Toolbox - "
# Describe what this menu does
	Description=" Ansible Management Menu "

# Display the menu options
show_menus(){
	echo " "	
	echo " $MenuTitle "
	echo " $Description "  
    echo " =========================================== "
	echo " -- Current Project: $JobID -- " 
    echo " =========================================== "
	echo "0.  Install Ansible"
	echo "01  Add a user to a group "
	echo "1.  configure SSH Keys "
	echo "2.  Configure Ansible " 
	echo "3.  List Hosts"
	echo "10. New Project"
	echo "11. Load a Project"
	echo "12. Validate Host Config "
	echo "13  Validate Remote config"
	echo "14. Edit project files"
	echo "15. Ping all servers & ask for ssh password"
	echo "20. Add a Remote User"
	echo "21. Add a Remote Server"
	echo "30. Run a playbook "
	echo "31. Validate a playbooks YML"
	echo "32. Create a playbook"
	echo "40. "
	echo "50. SSH Login test  "
	echo "51. Create SSH key "
	echo "52. Copy SSH key to a container"
	echo "53. Available containers"
	echo "60. "
	echo "70. Develeoping " 
    echo "80.  Display the last ten log entries."
	echo "99.  Quit			Exit this Menu"
	echo "-----------------------------------"
	echo " $lastmessage $Last10Logs " 
	 
}

read_options(){
# Maps the displayed options to command modules
	local choice
# Inform user how to proceed and capture input.	
	read -p "Enter the desired item number or command: " choice	
# Execute selected command modules	
	case $choice in
    0)   InstallAnsible ;;
	01)  AddUser2Group ;;
	1)   SetupSSHKeys ;;
	2)   ConfigureAnsible ;;
	3)   ListHosts ;;
	10)  NewProject ;;
	11)  LoadProject ;;
	12)  ansible -i $WorkDir --connection=local local -m ping ;;
	13)  ansible -i $WorkDir remote -m ping ;;
	14)  EditFiles ;;
	15)  PingAll ;;
	20)  AddRemoteUser ;;
	21)  AddPhysicalServer ;;
	30)	 RunPlaybook ;;
	31)  ValidateYML ;;
	32)  CreatePlaybook ;;
	40)  InstallAnsiblePIP ;;
	50)  SSHLogin ;;
	51)  AutoSSHKey ;;
	52)  SSHKey2Con ;;
	53)  status ;;
	60)   ;;
	61)   ;;
	70)  Developing ;;
	80)  Last10Logs ;;
    98)  UpdateMenu ;;
# Quit this menu
    99) clear && echo " See you later $USER! " && exit 0 ;;
    ACP) cd ~/anansible/ ; git add . ; git commit -m "pushed from DEV ENV"; git push ;;
# Capture non listed inputs and send to BASH.
	*) echo -e "${RED} $choice is not a displayed option, trying BaSH.....${STD}" && echo -e "$choice" | /bin/bash
	esac
}

# --------------- Main loop --------------------------
# Continues to iterate through this menu loop. 
while true
do
	show_menus
	read_options
done

}

# =================== Run Commands ===============
# commands to run in this file at start (or nothing will happen).
MainMenu

# https://computingforgeeks.com/deploy-kubernetes-cluster-on-ubuntu-with-kubeadm/
