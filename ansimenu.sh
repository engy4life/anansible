#!/bin/bash  

# ================ Menu Template =================
# A template with functional examples for creating BaSH menus.
# Save (or copy and paste) this script as your starting point for your file.


# ============ Environment Variables ============= 

# Options to change the printed text colour
STD='\033[0;0;39m'
RED='\033[0;41;30m'

# =================== Modules =====================
# Add your desired programme modules

UpdateMenu(){
# 
    cd $HOME/anansible
    git add .
    git commit -m "Auto Updates"
    git stash
	git push
    git pull 
}

DateTime(){
# create a variable which defines and executes the desired command (date).
	DT=$(date)
# Change text colour and display contents of the DT variable.
	echo -e "${RED} Today is: ${STD}" $DT
}

# ================== Main Menu ====================

MainMenu(){
# Clears the screen
	clear
# Define this menus title.	
	MenuTitle=" - Menu Template - "
# Describe what this menu does
	Description=" An empty template for new menus "

# Display the menu options
show_menus(){
	echo " "	
	echo " $MenuTitle "
	echo " $Description "  
	echo "-----------------------------------"
	echo "0.  Update this tool"
	echo "10. Anansible "
	echo "20. "
	echo "30. Run microkub installer "
	echo "40.  "
	echo "50. Show IP's "
	echo "60.  "
	echo "70.  " 
    echo "80.  "
    echo "90.  "
	echo "99.  Quit				   Exit this Menu"
	echo " "
	echo " $lastmessage " 
	echo " " 
}

read_options(){
# Maps the displayed options to command modules	
	local choice
# Inform user how to proceed and capture input.	
	read -p "Enter the desired item number or command: " choice
# Execute selected command modules	
	case $choice in
        0)   UpdateMenu ;;
		10)  $HOME/anansible/anansible.sh ;;
		20)   ;;
		30)  $HOME/anansible/microkub.sh ;;
		40)   ;;
		50)  ip r | grep src ;;
		60)   ;;
		70)   ;;
		80)   ;;
        90)   ;;
        ACP) cd ~/anansible/ ; git add . ; git commit -m "pushed from DEV ENV"; git push ;;
# Quit this menu
        99) clear && echo " See you later $USER! " && exit 0;;
# Capture non listed inputs and send to BASH.
		*) echo -e "${RED} $choice is not a displayed option, trying BaSH.....${STD}" && echo -e "$choice" | /bin/bash
	esac
}

# --------------- Main loop --------------------------
while true
do
	show_menus
	read_options
done

}

# =================== Run Commands ===============
# commands to run in this file at start.
MainMenu


